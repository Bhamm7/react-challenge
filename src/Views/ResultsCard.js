import React, { Component } from 'react';

class ResultsCard extends Component {
    
    render() {
        // Show a loading indicator instead of the entire results pane until it's ready
        if (this.props.isLoading){
            return <h1>Loading...</h1>
        }
      
        return ( 
        <div>
            {/* display the postal code or failure message */}
            <h1 className="result"> { this.props.result } </h1> 
            {/* toggle the class from hidden or show to hide data while on load or failure */}
            <div id="mp-card" className={this.props.resultVisibility}>
                <ul className = "column-2" >
                <li><h2> Name: { this.props.mpData.name } </h2></li>
                <li><h2> Email: { this.props.mpData.email } </h2></li>
                <li><h2> Party: { this.props.mpData.party } </h2></li>
                <li><h2> District: { this.props.mpData.district } </h2></li>
                </ul><img className="mp-photo column-2" src={ this.props.mpData.photo } alt="mp" />
            </div> 
            </div>
        )
    }

}
export default ResultsCard