import React, { Component } from 'react';
import ResultsCard from './Views/ResultsCard.js';
import logo from './logo.svg';
import './App.css';
import fetchJsonp from 'fetch-jsonp'

const API_URL = 'https://represent.opennorth.ca/postcodes/'

class App extends Component {
  constructor(props){
    super(props)

    this.state = {
      isLoading: false,
      searchText: '',
      mpData: {},
      result: "",
      resultVisibility: "hidden"
    }
  }

  // alters searchText value when the user types into the input
  handleSearchChange = (e) => {
    this.setState({searchText: e.target.value})
  }

  // validate the search text and warn the user if it's incorrect
  handleValidation = (searchTerm) => {
    var isValid = true
    
    // if the search text isn't of a proper postal code form, invalidate
    if (!/[A-Z]\d[A-Z]\d[A-Z]\d/.test(searchTerm)){
      console.log("Invalid postal code!")
      isValid = false
    }

    // if the text was invalid, show the red error message under the input field, reset it otherwise
    var errorMsg = document.getElementById("invalidPostal")
    if (isValid){
      errorMsg.classList.add("invisible")
    }else {
      errorMsg.classList.remove("invisible")
    }

    return isValid
  }

  // Nice to have: route the enter key to our onClick event
  keyPress = (e) => {
    if (e.keyCode === 13){
      this.getMpInfo(e)
    }
  }

  // main function that will fetch API data, update state fields if the data is valid, and control loading visibility
  getMpInfo = (e) => {
    e.preventDefault()
    // we're considered loading at this point, so display loading indicator
    this.setState({isLoading: true})

    var searchTerm = this.state.searchText
    // change the postal code to a standardized format, uppercase with no spaces
    searchTerm = searchTerm.toUpperCase().replace(/\s/g, "")
    // make another version to display to the user
    var displayPostal = searchTerm.replace(/(^\w\w\w)/, "$1 ")
    
    // only collect data if the postal code was of a valid format
    var isValid = this.handleValidation(searchTerm)
    if (isValid){
      // as soon as we know the input is valid, remove the current results pane
      this.setState({resultVisibility: "hidden"})
      // get API data
      fetchJsonp(API_URL + searchTerm)
        .then(response => {
          return response.json()
        }).then(json => {
          var MP = {}
          MP.name = json.representatives_centroid[0].name
          MP.email = json.representatives_centroid[0].email
          MP.photo = json.representatives_centroid[0].photo_url
          MP.party = json.representatives_centroid[0].party_name
          MP.district = json.representatives_centroid[0].district_name
          this.setState({mpData: MP, result: displayPostal, resultVisibility: "show"})
        }).catch(ex => {
          console.log('Parsing failed', ex)
          this.setState({result: "No Entries Found", isLoading: false})
        })
    }
    
    // reset the input box after each search, regardless of result
    this.setState({searchText: '', isLoading: false})
  }

  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">React Tech Challenge</h1>
        </header>
        <p className="App-intro">Enter your postal code to find your MP!</p>       
        <br />
        <input id="searchBox" className ="search-box" value={this.state.searchText} onKeyDown={this.keyPress} onChange={this.handleSearchChange}/>
        <p id="invalidPostal" className="error-string invisible">Invalid postal code</p>
        <br />
        <button className="search-button" onClick={this.getMpInfo}>Go</button>
        <ResultsCard {...this.state} />
        
      </div>
    );
  }
}

export default App;
